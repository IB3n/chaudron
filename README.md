# Chaudron

Pet project of a local restaurent frontend, beware that it's not a finished product, there is some remaining stuff to do, for instance :
* Preload database for development
* Migrations handling
* Error handling

## Tech stack
* MongoDB
* Strapi as backend
* React at frontend

## Getting started
It's possible to start the frontend and the backend alltogether with docker-compose
```sh
docker-compose up
```
Then you can access to the backend at [http://localhost:1337/admin](http://localhost:1337/admin), then frontend is available at [http://localhost:80](http://localhost:80)
To get a working frontend it's necessary to allow public access on the following resource in the admin dashboard (in Roles & Permissions > Public):
* news (count / find / findone)
* plats (count / find / findone)
* typeplats (count / find / findone)
