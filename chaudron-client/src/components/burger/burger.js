import './burger.scss';

import React, { Component } from 'react';

import sidebarStore from '../sidebar/sidebar.store';
import BurgerIcon from './burger-icon';

class Burger extends Component {
  openSidenav() {
    sidebarStore.dispatch({ type: "OPEN" });
  }

  render() {
    return (
      <button
        className="button button--icon"
        onClick={this.openSidenav}
        aria-label="Ouvrir la navigation"
      >
        <BurgerIcon />
      </button>
    );
  }
}

export default Burger;
