import './sidebar.scss';

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import normalizeName from '../../utils/normalize-name';
import typePlatStore from '../la-carte/type-plat/type-plat.store';
import CloseIcon from './close-icon';
import sidebarStore from './sidebar.store';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayed: sidebarStore.getState(),
      typePlats: typePlatStore.getState()
    };
    sidebarStore.subscribe(() => {
      const displayed = sidebarStore.getState();
      this.setState({ displayed });
      if (displayed) {
        document.querySelector(".close-button").focus();
      }
    });
    typePlatStore.subscribe(() => {
      this.setState({ typePlats: typePlatStore.getState() });
    });
  }

  sidebarClass() {
    return this.state.displayed ? "sidebar--shown" : "";
  }

  linkClicked() {
    sidebarStore.dispatch({ type: "CLOSE" });
  }

  createLink(typePlat, idx) {
    const delay = idx * 0.1 + "s";
    const linkStyle = {
      animationDelay: delay
    };
    let target = "#" + normalizeName(typePlat.label);
    let item;
    if (document.location.pathname === "/gallerie") {
      target = "/" + target;
      item = (
        <NavLink
          to={target}
          onClick={this.linkClicked}
          className="sidebar__link"
          tabIndex={this.state.displayed ? 0 : -1}
        >
          {typePlat.label}
        </NavLink>
      );
    } else {
      item = (
        <a
          href={target}
          onClick={this.linkClicked}
          className="sidebar__link"
          tabIndex={this.state.displayed ? 0 : -1}
        >
          {typePlat.label}
        </a>
      );
    }
    return typePlat.inNavbar ? (
      <li className="fade-in-left" key={typePlat._id} style={linkStyle}>
        {item}
      </li>
    ) : null;
  }

  createLinks() {
    return this.state.typePlats.map(this.createLink.bind(this));
  }

  closeSidebar() {
    sidebarStore.dispatch({ type: "CLOSE" });
  }
  x;

  render() {
    const sidebarClass = "bg-black white-text sidebar " + this.sidebarClass();
    const nbLink = this.state.typePlats.filter(e => e.inNavbar).length;
    const galleryLinkStyle = {
      animationDelay: (nbLink - 1) * 0.2 + "s"
    };
    return (
      <div
        className={sidebarClass}
        aria-hidden={this.state.displayed ? "false" : "true"}
      >
        <div className="text-container text-container--padding sidebar__actions d-flex flex-row justify-content-center align-items-end">
          <button
            className="button button--icon close-button"
            onClick={this.closeSidebar}
            tabIndex={this.state.displayed ? 0 : -1}
            aria-label="Fermer la navigation"
          >
            <CloseIcon />
          </button>
        </div>
        <ul className="d-flex flex-column align-items-center">
          {this.createLinks()}
          <li className="fade-in-left" style={galleryLinkStyle}>
            <NavLink
              to="/gallerie"
              onClick={this.linkClicked}
              className="sidebar__link"
              tabIndex={this.state.displayed ? 0 : -1}
            >
              Gallerie
            </NavLink>
          </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
