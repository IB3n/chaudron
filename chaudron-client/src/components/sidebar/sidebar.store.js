import { createStore } from 'redux';

function sidebarReducer(state = false, action) {
  switch (action.type) {
    case "OPEN":
      return true;
    case "CLOSE":
      return false;
    default:
      return state;
  }
}

const sidebarStore = createStore(sidebarReducer);

export default sidebarStore;
