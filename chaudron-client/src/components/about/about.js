import './about.scss';

import React, { Component } from 'react';

class About extends Component {
  render() {
    const aboutClass = this.props.className + " about py-2";
    return (
      <div className={aboutClass}>
        <div className="d-flex flex-column justify-content-evenly">
          <div className="about__adresse">
            2 rue de la mairie 35320 CREVIN
            <div className="about__phone">
              <a href="tel:0954205330">09 54 20 53 30</a>
            </div>
          </div>
          <section className="about__section">
            <header className="about__header">Restaurant</header>
            <div className="about__description">
              Plat du jour tous les midis du lundi au vendredi
            </div>
          </section>
          <section className="about__section">
            <header className="about__header">Crêperie</header>
            <div className="about__description">A chaque service</div>
          </section>
          <section className="about__section">
            <header className="about__header">Pizza - Burger</header>
            <div className="about__description">
              Sur place ou à emporter les vendredis et les samedis soir
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default About;
