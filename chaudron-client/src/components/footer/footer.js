import './footer.scss';

import React from 'react';

const Footer = () => (
  <footer className="page-footer">
    <div className="text-container text-container--padding d-flex flex-wrap flex-row">
      <div className="flex-33 m-align-center align-left">
        <div className="white-text">Restaurant Pizzeria Le Chaudron</div>
        <div>2 Rue de la Mairie</div>
        <div>35320 Crevin</div>
        <a className="grey-text text-lighten-4" href="tel:0954205330">
          09 54 20 53 30
        </a>
        <div className="show-mobile" aria-hidden="true">
          © 2019 Le Chaudron
        </div>
      </div>
      <div className="hide-mobile d-flex flex-33 justify-content-center">
        <div>© 2019 Le Chaudron</div>
      </div>
      <div className="flex-33">
        <div className="m-align-center align-right">
          Designé et développé par Benjamin&nbsp;MERTZ
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
