import './la-carte.scss';

import React, { Component } from 'react';

import TypePlat from './type-plat/type-plat';
import typePlatStore from './type-plat/type-plat.store';

class LaCarte extends Component {
  constructor(props) {
    super(props);
    this.state = { typePlats: typePlatStore.getState() };
    typePlatStore.subscribe(data => {
      this.setState({ typePlats: typePlatStore.getState() });
    });
  }

  scrollToHash() {
    const hash = window.location.hash;
    if (hash && document.querySelector(hash)) {
      document.querySelector(hash).scrollIntoView();
    }
  }

  componentDidUpdate() {
    this.scrollToHash();
  }

  componentDidMount() {
    this.scrollToHash();
  }

  render() {
    const typePlats = this.state.typePlats.map((typePlat, idx) => (
      <TypePlat key={idx} typePlat={typePlat} />
    ));
    return (
      <div>
        <div className="invert-bar py-2">
          <h1 className="text-container text-container--padding h1">
            La Carte
          </h1>
        </div>
        <div className="list-type-plats">{typePlats}</div>
      </div>
    );
  }
}

export default LaCarte;
