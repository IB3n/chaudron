import './type-plat.scss';

import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import displayPrice from '../../../utils/display-price';
import normalizeName from '../../../utils/normalize-name';

class TypePlat extends Component {
  createPlatItem(plat) {
    return (
      <div className="plat" key={plat._id}>
        <div className="plat__header">
          <h3 className="plat__nom">{plat.label}</h3>
          <div className="plat__grower" />
          <span>{displayPrice(plat.prix)}</span>
        </div>
        <div className="plat__description">{plat.description}</div>
      </div>
    );
  }

  createListePlats(listePlats) {
    return (
      <div className="list-plats text-container">
        {listePlats.map(this.createPlatItem.bind(this))}
      </div>
    );
  }

  createDescription(typePlat) {
    return typePlat.description ? (
      <div className="type-plat__description text-container">
        {typePlat.description}
      </div>
    ) : null;
  }

  createPlatStyle(typePlat) {
    if (typePlat.image && typePlat.image.url) {
      return {
        backgroundImage: `url(/api/${typePlat.image.url})`
      };
    } else {
      return null;
    }
  }

  render() {
    let imageTarget;
    const typePlat = this.props.typePlat;
    if (typePlat.image) {
      imageTarget = `/api/${this.props.typePlat.image.url}`;
    }
    return (
      <section>
        <div
          // style={this.createPlatStyle(this.props.typePlat)}
          className="type-plat__header"
          id={normalizeName(this.props.typePlat.label)}
        >
          <LazyLoadImage
            className="type-plat__image"
            alt=""
            src={imageTarget}
            height="100%"
            width="100%"
          />
          <div className="header__overlay" />
          <h2 className="type-plat__title text-container">
            {this.props.typePlat.label}
          </h2>
        </div>
        {this.createDescription(this.props.typePlat)}
        {this.createListePlats(this.props.typePlat.plats)}
      </section>
    );
  }
}

export default TypePlat;
