import { createStore } from 'redux';

function typePlatReducer(state = [], action) {
  switch (action.type) {
    case "SET":
      return action.data;
    default:
      return state;
  }
}

const typePlatStore = createStore(typePlatReducer);

export default typePlatStore;
