import './news.scss';

import React, { Component } from 'react';

import ellipsis from '../../utils/ellipsis';

class News extends Component {
  constructor(props) {
    super(props);
    this.state = { news: [] };
  }

  componentDidMount() {
    fetch("/api/news")
      .then(results => results.json())
      .then(news => {
        this.setState({ news });
      });
  }

  buildPieceOfNewsPost(pieceOfNews) {
    const newsComponentClass = "news__item white-text";
    return (
      <div key={pieceOfNews._id} className={newsComponentClass}>
        <div className="news__content">
          <header className="text-container text-container--padding">
            <h2 className="my-0 h2">{pieceOfNews.label}</h2>
          </header>
          <div className="text-container text-container--padding news__description">
            {ellipsis(pieceOfNews.description, 288)}
          </div>
        </div>
      </div>
    );
  }

  buildNewsPosts(news) {
    return (news || []).map(this.buildPieceOfNewsPost.bind(this));
  }

  buildNews() {
    let newsStyle;
    if (this.state.news[0] && this.state.news[0].image) {
      const firstPieceOfNews = this.state.news[0];
      newsStyle = {
        backgroundImage: `url(/api/${firstPieceOfNews.image.url})`
      };
    } else {
      newsStyle = {};
    }
    return (
      <div className="news" style={newsStyle}>
        <div className="news__overlay" />
        {this.buildNewsPosts(this.state.news)}
      </div>
    );
  }

  render() {
    return (
      <div className={this.props.className}>
        <div className="invert-bar py-2">
          <h1 className="text-container text-container--padding h1">
            Les nouvelles
          </h1>
        </div>
        {this.buildNews()}
      </div>
    );
  }
}

export default News;
