import './header.scss';

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import normalizeName from '../../utils/normalize-name';
import Burger from '../burger/burger';
import typePlatStore from '../la-carte/type-plat/type-plat.store';
import logo from './logo.png';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { typePlats: [] };
    typePlatStore.subscribe(() => {
      this.setState({ typePlats: typePlatStore.getState() });
    });
  }

  createLink(typePlat) {
    let target = "#" + normalizeName(typePlat.label);
    let item;
    if (document.location.pathname === "/gallerie") {
      target = "/" + target;
      item = (
        <NavLink to={target} className="button">
          {typePlat.label}
        </NavLink>
      );
    } else {
      item = (
        <a href={target} className="button">
          {typePlat.label}
        </a>
      );
    }
    return typePlat.inNavbar ? <li key={typePlat._id}>{item}</li> : null;
  }

  createLinks() {
    return this.state.typePlats.map(this.createLink.bind(this));
  }

  render() {
    return (
      <nav className="nav d-flex align-items-center justify-conten-center pt-2 pb-1">
        <div className="text-container d-flex flex-row">
          <div className="d-flex m-flex-row align-items-center mflex-justify-conten-between">
            <div className="logo__container">
              <img src={logo} className="logo" alt="" />
            </div>
            <span className="nav__brand px-2">
              <span className="hide-mobile nav__verbose-name">
                Restaurant Crêperie Pizzeria{" "}
              </span>
              Le Chaudron
            </span>
            <div className="show-mobile d-flex flex-column align-items-center justify-content-center">
              <Burger />
            </div>
          </div>
          <ul id="nav-mobile" className="d-flex flex-row hide-mobile">
            {this.createLinks()}
            <li>
              <NavLink to="/gallerie" className="button">
                Gallerie
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Header;
