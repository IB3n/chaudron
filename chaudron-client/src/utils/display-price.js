const displayPrice = (price, symbol) => {
  symbol = symbol ? symbol : "€";
  return price.toFixed(2) + " " + symbol;
};

export default displayPrice;
