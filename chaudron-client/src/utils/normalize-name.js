const normalizeName = text => {
  text = text
    ? text
        .toLowerCase()
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
    : "";
  const normalizeRe = new RegExp(/[ '",]/, "g");
  return text.replace(normalizeRe, "");
};

export default normalizeName;
