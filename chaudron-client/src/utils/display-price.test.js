import displayPrice from './display-price';

describe("display-price", () => {
  it("10 returns 10.00 €", () => {
    expect(displayPrice(10, "€")).toEqual("10.00 €");
  });
  it("symbol defaults on €", () => {
    expect(displayPrice(10)).toEqual("10.00 €");
  });
  it("symbol can be changed", () => {
    expect(displayPrice(10, "yen")).toEqual("10.00 yen");
  });
  it("10.5 returns 10.50 €", () => {
    expect(displayPrice(10.5, "€")).toEqual("10.50 €");
  });
});
