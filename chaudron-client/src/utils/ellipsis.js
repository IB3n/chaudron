const ellipsis = (text, nb) => {
  nb = nb ? nb : 144;
  if (text.length > nb) {
    return text.slice(0, 144) + "…";
  } else {
    return text;
  }
};

export default ellipsis;
