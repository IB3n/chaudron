import normalizeName from './normalize-name';

describe("normalize-name", () => {
  it("normalize-name supprime les espaces", () => {
    expect(normalizeName("hello world")).toEqual("helloworld");
  });
  it("normalize-name met en lowerCase le texte", () => {
    expect(normalizeName("HELLO")).toEqual("hello");
  });

  it("s'execute pour chaque occurance", () => {
    expect(normalizeName("HELLO HELLO HELLO")).toEqual("hellohellohello");
  });
});
