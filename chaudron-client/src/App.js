import './App.scss';

import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Footer from './components/footer/footer';
import Header from './components/header/header';
import typePlatStore from './components/la-carte/type-plat/type-plat.store';
import Sidebar from './components/sidebar/sidebar';
import PGallery from './pages/gallery/gallery';
import PMain from './pages/main/main';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { typePlats: [] };
  }

  componentDidMount() {
    fetch("/api/typeplats")
      .then(results => results.json())
      .then(typePlats => {
        this.setState({ typePlats });
        typePlatStore.dispatch({ type: "SET", data: typePlats });
      });
  }

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Header typePlats={this.state.typePlats} />
          <Sidebar />
          <main>
            <Route
              path="/"
              exact
              render={props => (
                <PMain {...props} typePlats={this.state.typePlats} />
              )}
            />
            <Route path="/gallerie" exact component={PGallery} />
          </main>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
