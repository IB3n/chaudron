import './gallery.scss';

import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

class PGallery extends Component {
  constructor(props) {
    super(props);
    this.state = { photos: [] };
  }

  setPhotosInState(photos) {
    this.setState({ photos });
  }

  componentDidMount() {
    document.title = "Gallerie | Le Chaudron - Restaurant Crêperie Pizzeria";
    fetch("/api/photos")
      .then(response => response.json())
      .then(this.setPhotosInState.bind(this));
  }

  buildImage(photo) {
    const target = `/api${photo.image.url}`;
    return (
      <div className="photo__container" key={photo._id}>
        <LazyLoadImage
          className="type-plat__image"
          alt=""
          src={target}
          height="100%"
          width="100%"
        />
        <div className="photo__overlay align-items-center justify-content-center">
          <div className="photo__description">{photo.description}</div>
        </div>
      </div>
    );
  }

  buildImages() {
    return this.state.photos.map(this.buildImage.bind(this));
  }

  render() {
    return (
      <div className="bg-black">
        <div className="text-container text-container--padding">
          <h1 className="my-0 h1">Gallerie</h1>
          <div className="d-flex flex-wrap">{this.buildImages()}</div>
        </div>
      </div>
    );
  }
}

export default PGallery;
