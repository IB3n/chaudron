import React, { Component } from 'react';

import About from '../../components/about/about';
import LaCarte from '../../components/la-carte/la-carte';
import News from '../../components/news/news';

class PMain extends Component {
  componentDidMount() {
    document.title = "Accueil | Le Chaudron - Restaurant Crêperie Pizzeria";
  }

  render() {
    return (
      <div>
        <div className="bg-black">
          <div className="text-container d-flex flex-row ">
            <About className="flex-50 right-container--padding left-container--padding" />
            <News />
          </div>
        </div>
        <LaCarte typePlats={this.props.typePlats} />
      </div>
    );
  }
}

export default PMain;
