const proxy = require("http-proxy-middleware");

const options = {
  target: "http://localhost:1337/",
  pathRewrite: {
    "^/api": "/"
  }
};

module.exports = function(app) {
  app.use(proxy("/api/", options));
};
