var Slideshow = function(params) {
  // définition des variables internes
  var currentDiffusion,
    currentPubs,
    currentPubIndex,
    imageElement,
    imageContainer,
    _timeout;

  currentDiffusion = {};
  currentPubs = [];
  currentPubIndex = 0;
  imageElement = document.querySelector(".image-container img");
  imageContainer = document.querySelector(".background");

  var log = function(args) {
    params.debug && console.log(args);
  };

  // méthodes privées
  var _getDiffusions = function() {
    log("[GET DIFFUSIONS] called");
    return fetch(params.diffusionsURL).then(response => response.json());
  };
  var _getPubs = function() {
    log("[GET PUBS] called");
    return fetch(params.pubsURL).then(response => response.json());
  };
  var _registerDiffusion = function(diffusions) {
    log("[REGISTER DIFFUSION] called");
    currentDiffusion = diffusions[0];
    log("[REGISTER DIFFUSION] setting new backgroundImage");
    imageContainer.style.setProperty(
      "background-image",
      "url(/api" + diffusions[0].background.url + ")"
    );
    return Promise.resolve(currentDiffusion);
  };
  var _registerPubs = function(pubs) {
    log("[REGISTER PUBS] called for", pubs);
    currentPubs = pubs;
    Promise.resolve();
  };

  // méthodes publiques
  var startSlideShow = function() {
    log("[START SLIDESHOW] called");
    imageElement.setAttribute("src", "/api" + currentPubs[0].image.url);
    nextSlide(currentDiffusion.secondes * 1000);
  };

  var removeImage = function(cb) {
    log("[REMOVE IMAGE] removing current image");
    log("[REMOVE IMAGE] remove animation in");
    log("[REMOVE IMAGE] add animation out");
    imageElement.classList.remove(params.animationIn);
    imageElement.classList.add(params.animationOut);
    var postAnimationHook = function(ev) {
      log("[REMOVE IMAGE] [POST ANIMATION HOOK FN] called");
      imageElement.removeEventListener("animationend", postAnimationHook);
      cb();
    };
    imageElement.addEventListener("animationend", postAnimationHook);
  };

  var setNewImage = function(url) {
    log(`[SET IMAGE] setting new image ${url}`);
    log("[SET IMAGE] removing animationOut", url);
    imageElement.classList.remove(params.animationOut);
    log("[SET IMAGE] adding animationIn", url);
    imageElement.classList.add(params.animationIn);
    log("[SET IMAGE] setting source", url);
    imageElement.setAttribute("src", "/api" + url);
  };

  var _isCurrentSlideTheLastOne = function() {
    return currentPubs.length === currentPubIndex + 1;
  };

  var nextSlide = function(delay) {
    log(`[NEXT SLIDE] called with delay ${delay}`);
    if (_isCurrentSlideTheLastOne()) {
      currentPubIndex = 0;
    } else {
      currentPubIndex++;
    }
    removeImage(function() {
      log("[NEXT SLIDE] setting new image");
      setNewImage(currentPubs[currentPubIndex].image.url);
    });
    log(
      `[NEXT SLIDE] clear timeout ${_timeout} ---------------------------------------------|`
    );
    clearTimeout(_timeout);
    _timeout = setTimeout(function() {
      log("[NEXT SLIDE] timeout triggered");
      nextSlide(delay);
    }, delay);
  };

  var refreshDiffusionInContext = function() {
    return _getDiffusions()
      .then(_registerDiffusion.bind(this))
      .then(_getPubs)
      .then(_registerPubs.bind(this));
  };

  return {
    refreshDiffusionInContext: refreshDiffusionInContext,
    nextSlide: nextSlide,
    startSlideShow: startSlideShow
  };
};
