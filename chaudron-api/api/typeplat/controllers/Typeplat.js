'use strict';

/**
 * Typeplat.js controller
 *
 * @description: A set of functions called "actions" for managing `Typeplat`.
 */

module.exports = {

  /**
   * Retrieve typeplat records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.typeplat.search(ctx.query);
    } else {
      return strapi.services.typeplat.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a typeplat record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.typeplat.fetch(ctx.params);
  },

  /**
   * Count typeplat records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.typeplat.count(ctx.query, populate);
  },

  /**
   * Create a/an typeplat record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.typeplat.add(ctx.request.body);
  },

  /**
   * Update a/an typeplat record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.typeplat.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an typeplat record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.typeplat.remove(ctx.params);
  }
};
