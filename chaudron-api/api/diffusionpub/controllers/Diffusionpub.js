'use strict';

/**
 * Diffusionpub.js controller
 *
 * @description: A set of functions called "actions" for managing `Diffusionpub`.
 */

module.exports = {

  /**
   * Retrieve diffusionpub records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.diffusionpub.search(ctx.query);
    } else {
      return strapi.services.diffusionpub.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a diffusionpub record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.diffusionpub.fetch(ctx.params);
  },

  /**
   * Count diffusionpub records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.diffusionpub.count(ctx.query, populate);
  },

  /**
   * Create a/an diffusionpub record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.diffusionpub.add(ctx.request.body);
  },

  /**
   * Update a/an diffusionpub record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.diffusionpub.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an diffusionpub record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.diffusionpub.remove(ctx.params);
  }
};
