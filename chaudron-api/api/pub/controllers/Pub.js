'use strict';

/**
 * Pub.js controller
 *
 * @description: A set of functions called "actions" for managing `Pub`.
 */

module.exports = {

  /**
   * Retrieve pub records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.pub.search(ctx.query);
    } else {
      return strapi.services.pub.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a pub record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.pub.fetch(ctx.params);
  },

  /**
   * Count pub records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.pub.count(ctx.query, populate);
  },

  /**
   * Create a/an pub record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.pub.add(ctx.request.body);
  },

  /**
   * Update a/an pub record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.pub.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an pub record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.pub.remove(ctx.params);
  }
};
